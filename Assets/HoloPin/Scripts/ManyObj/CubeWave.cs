﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

public class CubeWave : MonoBehaviour
{
    [SerializeField]
    Shader kernelShader;
    [SerializeField]
    Shader debugShader;
    [SerializeField]
    Mesh[] shapes = new Mesh[1];
    [SerializeField]
    Material material;
    [SerializeField]
    ShadowCastingMode castShadows;
    [SerializeField]
    bool receiveShadows = false;
    [SerializeField]
    bool debug = false;

    RenderTexture positionBuffer1;
    RenderTexture positionBuffer2;
    Material kernelMaterial;
    Material debugMaterial;
    MaterialPropertyBlock props;
    BulkMesh bulkMesh;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {

    }
}
